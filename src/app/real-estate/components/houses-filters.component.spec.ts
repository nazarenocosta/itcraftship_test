import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup, FormControl } from '@angular/forms';
import { HousesFiltersComponent } from './houses-filters.component';

describe('HousesFiltersComponent', () => {
  let component: HousesFiltersComponent;
  let fixture: ComponentFixture<HousesFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HousesFiltersComponent],
      imports: []
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousesFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create filters form', () => {
    expect(component.form instanceof FormGroup).toBe(true);
  });

  // TODO(1pts)
  it('form should have cityId control', () => {
    expect(component.form.controls['cityId'] instanceof FormControl).toBe(true);
  });

  // TODO(1pts)
  it('should render cityId control', () => {});

  // TODO(1pts)
  it('form should have priceLessThan control', () => {
    expect(component.form.controls['priceLessThan'] instanceof FormControl).toBe(true);
  });

  // TODO(1pts)
  it('should render priceLessThan control', () => {});

  // TODO(1pts)
  it('form should have onSale control', () => {
    expect(component.form.controls['onSale'] instanceof FormControl).toBe(true);
  });

  // TODO(1pts)
  it('should render onSale control', () => {});
});
