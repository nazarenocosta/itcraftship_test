import { Component, Input } from '@angular/core';
import { House } from '../models';

/* TODO(5pts): render houses list */
@Component({
  selector: 'app-houses-list',
  template: `
    <table class="table">
      <thead>
        <tr>
          <th>Id</th>
          <th>Title</th>
          <th>Price</th>
          <th>OnSale</th>
        </tr>
      </thead>
      <tbody>
        <tr *ngFor="let item of houses" [routerLink]="['/real-estate/', item.id]">
          <td>{{item.id}}</td>
          <td>{{item.title}}</td>
          <td>{{item.price}}</td>
          <td>{{item.onSale}}</td>
        </tr>
      </tbody>
    </table>
  `,
  styles: []
})
export class HousesListComponent {
  @Input()
  houses: House[];
}
