import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { City, HouseFilters } from '../models';

/* TODO(5pts): create form controls */
/* TODO(5pts): render form */

@Component({
  selector: 'app-houses-filters',
  template: `
  <ngb-accordion #acc="ngbAccordion" activeIds="ngb-panel-0">
    <ngb-panel title="Filter">
      <ng-template ngbPanelContent>
        <form [formGroup]="form" (ngSubmit)="onSubmit()">
          <div class="form-group">
            <label for="cityId">City</label>
            <select id="cityId" class="form-control" formControlName="cityId">
              <option value=""></option>
              <option *ngFor="let city of cities" [value]="city.id">{{city.name}}</option>
            </select>
          </div>

          <div class="form-group">
            <label for="priceLessThan">Price Less Than</label>
            <input id="priceLessThan" formControlName="priceLessThan" type="number" class="form-control">
          </div>

          <div class="form-group">
            On Sale:<br>
            <input type="radio" formControlName="onSale" value="all"> All<br>
            <input type="radio" formControlName="onSale" value="yes"> Yes<br>
            <input type="radio" formControlName="onSale" value="no"> No
          </div>

          <button color="primary">Search</button>
        </form>
      </ng-template>
    </ngb-panel>
  </ngb-accordion>`
})
export class HousesFiltersComponent {
  @Output()
  filtersChange = new EventEmitter<HouseFilters>();

  @Input()
  set filters(v: HouseFilters) {
    this.form.patchValue(v || {});
  }
  @Input()
  cities: City[];

  form = new FormGroup({
    cityId: new FormControl(''),
    priceLessThan: new FormControl(''),
    onSale: new FormControl('')
  });

  onSubmit() {
    this.filtersChange.emit(this.form.value);
  }
}
