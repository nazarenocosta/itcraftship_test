import { of } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CitiesService } from '../apis/cities.service';
import { HousesService } from '../apis/houses.service';
import { City, House, HouseFilters } from '../models';

@Component({
  selector: 'app-house-listings',
  template: `
    <app-houses-filters
      [cities]="cities$ | async"
      [filters]="filters$ | async"
      (filtersChange)="onFiltersChange($event)">
    </app-houses-filters>

    <app-houses-list
      [houses]="houses$ | async">
    </app-houses-list>
  `,
  styles: [
    `
      :host {
        display: block;
      }
    `
  ]
})
export class HouseListingsComponent implements OnInit {
  cities$: Observable<City[]>;
  filters$: Observable<HouseFilters>;
  houses$: Observable<House[]>;

  constructor(
    private citiesAPI: CitiesService,
    private houseAPI: HousesService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  getFilters(): Observable<HouseFilters> {
    let filters: HouseFilters;
    filters = {};
    this.activatedRoute.queryParams.subscribe((params) => {
      if ((params['onSale'] === 'all') || (params['onSale'] === 'yes')) {
        filters.onSale = true;
      } else if (params['onSale'] === 'no') {
        filters.onSale = false;
      } else {
        filters.onSale = undefined;
      }

      filters.cityId = parseInt(params['cityId'], 2) || undefined;
      filters.priceLessThan = params['priceLessThan'] || undefined;
    });
    return of(filters);
  }

  ngOnInit() {
    /*
    1.TODO(1pts)
      Goal: fetch all cities
      Implementation: set this.cities$

    2. TODO(5pts)
      Goal: parse query params stream into filters object
      Implementation: set this.filters$
      Hint:
        query params: real-estate?cityId=3&onSale=false&priceLessThan=244
        parsed value: { cityId: 3, onSale: false, priceLessThan: 244 }

    3. TODO(8pts)
      Goal: fetch all houses matching current filters
      Implementation: set this.houses$
      Hint: this example includes using higher order observables.
            we must switch from the filters$ stream into the houses$ stream.

    */
    this.filters$ = this.getFilters();
    console.log(this.filters$);
    this.cities$ = this.citiesAPI.getCities();

    this.filters$.subscribe(houseFilters => {
      console.log(houseFilters);
      this.houses$ = this.houseAPI.getHouses(houseFilters);
    });

    console.log(this.houses$);
  }

  onFiltersChange(queryParams: HouseFilters) {
    /* TODO(1pts)
      Goal: update URL query params with the new filters
    */
   console.log('NavigateByUrl');
   this.router.navigate(['/real-estate'], {queryParams});
  }
}
